INSERT INTO Integrante (nombre, apellido, matricula, rol, codigo, url)
VALUES ('Ruben ', 'Ocampos', 'Y12700', 'Desarrollador', '00', 'Y12700/index.html');

INSERT INTO Integrante (nombre, apellido, matricula, rol, codigo, url)
VALUES ('Juan ', 'Aquino', 'Y19937', 'Desarrollador', '01', 'Y19937/index.html');

INSERT INTO Integrante (nombre, apellido, matricula, rol, codigo, url)
VALUES ('Junior  ', 'Cabral', 'Y25387', 'Desarrollador', '02', 'Y25387/index.html');

INSERT INTO Integrante (nombre, apellido, matricula, rol, codigo, url)
VALUES ('Sebastian ', 'Duarte', 'Y25495', 'Desarrollador', '03', 'Y25495/index.html');

INSERT INTO Integrante (nombre, apellido, matricula, rol, codigo, url)
VALUES ('Elvio ', 'Aguero', 'Y19099', 'Desarrollador', '04', 'Y19099/index.html');

INSERT INTO Integrante (nombre, apellido, matricula, rol, codigo, url)
VALUES ('Luis ', 'Delgado', 'UG0085', 'Desarrollador', '05', 'UG0085/index.html');
-- insert tipos medio

INSERT OR IGNORE INTO TipoMedio (tipo, descripcion)
VALUES
  ('Youtube', 'Videos en linea'),
  ('Twitter/X', 'Mensajes cortos y multimedia'),
  ('Dibujo', 'Ilustraciones y gráficos');

-- Insert Ruben
INSERT INTO Media (url, matricula, titulo, parrafo, imagen, video, html, tipo_medio)
VALUES
  ('Y12700/index.html', 'Y12700', 'Mi Video Favorito en YouTube.', 'Un buen video explicando el tema de los hackers.', '', 'https://www.youtube.com/watch?v=UVQrM86zc30', '', 'Youtube'),
  ('Y12700/index.html', 'Y12700', 'Imagen Favorita', 'Esta imagen representa mi color favorito.', '../../assets/Ruben imagen.jpg', '', '', 'Imagen'),
  ('Y12700/index.html', 'Y12700', 'Dibujo Propio', 'Algo que me apasiona bastante, además de la física de partículas y los atardeceres, es poder mirar más allá de donde estamos, imaginar los mundos posibles en la vastedad del cosmos y lo impresionante que son, como este dibujo de un planeta con un sistema de anillos parecido al de Júpiter.', '../../assets/Junior-Dibujo.png', '', '', 'Dibujo');

-- Insert Junior
INSERT INTO Media (url, matricula, titulo, parrafo, imagen, video, html, tipo_medio)
VALUES
  ('Y25387/index.html', 'Y25387', 'Mi Video Favorito en YouTube.', 'Uno de los vídeos más interesantes para mí trata sobre la idea preconcebida de que estamos hechos de partículas, sino de algo más, tal vez campos que vibran en el espacio y en base a esas fluctuaciones existimos.', '', 'https://www.youtube.com/embed/Tsnyq-3k7Bg?si=-Bzir8axv4WRU4MS&amp;controls=0', '', 'Youtube'),
  ('Y25387/index.html', 'Y25387', 'Imagen Favorita', 'Una de las cosas que más me gusta hacer es observar el atardecer. Es un momento de paz y tranquilidad que me ayuda a relajarme y pensar en las cosas que me importan, nunca dejando de cuestionarme los hechos físicos que llevan a que se puedan observar, como en esta imagen', '../../assets/Junior%20-%20Foto.jpg', '', '', 'Imagen'),
  ('Y25387/index.html', 'Y25387', 'Dibujo Propio', 'Algo que me apasiona bastante, además de la física de partículas y los atardeceres, es poder mirar más allá de donde estamos, imaginar los mundos posibles en la vastedad del cosmos y lo impresionante que son, como este dibujo de un planeta con un sistema de anillos parecido al de Júpiter.', '../../assets/Junior-Dibujo.png', '', '', 'Dibujo');

-- Insert Sebastian
INSERT INTO Media (url, matricula, titulo, parrafo, imagen, video, html, tipo_medio)
VALUES
  ('Y25495/index.html', 'Y25495', 'Mi Video Favorito en YouTube.', 'Mi video favorito es el trailer de la última temporada de mi serie favorita, considerada una obra maestra narrativa y visual. Para mí, simboliza el fin de una etapa en mi vida, ya que seguí esta serie desde hace 8 años.', '', 'https://www.youtube.com/embed/FRn6xXXF-7s?si=yVJP3egE0MB4siuE', '', 'Youtube'),
  ('Y25495/index.html', 'Y25495', 'Imagen Favorita', 'Mi imagen favorita es el protagonista del videojuego Dark Souls, y la imagen representa para mí su desesperanza y su impotencia, pero al mismo tiempo el objetivo que lo mantiene en su travesía. Me gusta por todo lo que representa para mí y todo lo significó para mí el juego y su historia bien elaborada.', '../../assets/Sebastian-foto.webp', '', '', 'Imagen'),
  ('Y25495/index.html', 'Y25495', 'Dibujo Propio', 'Mi dibujo siento que representa la perseverancia, ya que esta fogata es aquella en la que los aventureros descansan y se arman de valor para seguir con su viaje. También está sacada del videojuego Dark Souls.', '../../assets/Sebastian-Dibujo.jpg', '', '', 'Dibujo'),

-- Insert Elvio
INSERT INTO Media (url, matricula, titulo, parrafo, imagen, video, html, tipo_medio)
VALUES
  ('', 'Y19099', 'Mi Video Favorito en YouTube.', '', '', 'https://www.youtube.com/embed/NynNdkY2gx0?si=nzQjEF5y6hY6uSOp', '', 'Youtube'),
  ('Y19099/index.html', 'Y19099', 'Imagen Favorita', '', '../../assets/Elvio-Foto.jpg', '', '', 'Imagen'),
  ('Y19099/index.html', 'Y19099', 'Dibujo Propio', '', '../../assets/Elvio-Dibujo.png', '', '', 'Dibujo');

-- Insert Luis
INSERT INTO Media (url, matricula, titulo, parrafo, imagen, video, html, tipo_medio)
VALUES
  ('UG0085/index.html', 'UG0085', 'Mi Video Favorito en YouTube.', 'Uno de los vídeos que más me llama la atención es acerca de un libro que se llama La Vida Secreta De La Mente.', '', 'https://www.youtube.com/embed/Hv5ET7azgEk?si=WxryTH3oIMGTTD-Z', '', 'Youtube'),
  ('UG0085/index.html', 'UG0085', 'Imagen Favorita', 'Una de las cosas que más me gusta es escalar cerros y observar los hermosos paisajes de nuestro país. Cerro Hu', '../../assets/Luis Delgado.jpeg', '', '', 'Imagen'),
  ('UG0085/index.html', 'UG0085', 'Dibujo Propio', 'Siempre es importante aprender algo nuevo.', '../../assets/Luis Delgado-2.png', '', '', 'Dibujo');


INSERT INTO Media (id, url, matricula, titulo, parrafo, imagen, video, html, tipo_medio)
VALUES (NULL, '/', 'Home', 'Pagina Grupo 01', 'Nombres y Matricula', 'Logo Del grupo', './assets/logo.jpeg', '', 'tipo_de_medio');
